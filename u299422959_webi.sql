-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Tempo de geração: 24/01/2019 às 11:25
-- Versão do servidor: 10.2.17-MariaDB
-- Versão do PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `u299422959_webi`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `email_actions`
--

CREATE TABLE `email_actions` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `callback` text COLLATE utf8_unicode_ci NOT NULL,
  `expiry` text COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` text COLLATE utf8_unicode_ci NOT NULL,
  `token` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `email_actions`
--

INSERT INTO `email_actions` (`id`, `name`, `value`, `callback`, `expiry`, `updated_at`, `created_at`, `token`) VALUES
(1, 'password-remember', 'lucas@gmail.com', 'recover_user', '1547171798', '2019-01-11 01:56:38', '2019-01-11 01:56:38', ''),
(2, 'password-remember', 'lucas@gmail.com', 'recover_user', '1547172017', '2019-01-11 02:00:17', '2019-01-11 02:00:17', '488d6faa0fdad0b7cc18e1e941fa858f'),
(3, 'password-remember', 'lucas@gmail.com', 'recover_user', '1547172410', '2019-01-11 02:06:50', '2019-01-11 02:06:50', '18074cbb9451faa2e3b0a6ce733619c8'),
(4, 'password-remember', 'lucas@gmail.com', 'recover_user', '1547172449', '2019-01-11 02:07:29', '2019-01-11 02:07:29', '7f33a7f431f86510e237d0454cae8be9');

-- --------------------------------------------------------

--
-- Estrutura para tabela `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `role_users`
--

CREATE TABLE `role_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `section_login`
--

CREATE TABLE `section_login` (
  `id` int(11) NOT NULL,
  `user_id` text NOT NULL,
  `time` text NOT NULL,
  `token` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `section_login`
--

INSERT INTO `section_login` (`id`, `user_id`, `time`, `token`) VALUES
(1, '3', '31201am0149', '094f03a41f0eb0cd7186a74e22576363'),
(2, '4', '41201am0510', 'ef201e27e65a7a5fbdb837ab2e8aa41e'),
(3, '5', '51201am2009', 'c05991b3d8b575cc2535aa51e2d65727'),
(4, '6', '61201pm5633', '8f688c53ea09bee0ce6bc130eee50158'),
(5, 'zz', '1547308603', 'ccb5222fb8792eaa39f2210ca4c69d9e'),
(6, 'zz', '1547308603', 'ccb5222fb8792eaa39f2210ca4c69d9e'),
(7, '7', '71201pm5034', 'c82282475d6b43c1f5079cd06186e362'),
(8, 'a@b.com', '1547319136', '33d8b90a84bbd1ff1915386d5cc0c050'),
(9, 'a@b.com', '1547319151', '7339344251558ce3c5c844080f6bc5e9'),
(10, '8', '81201pm1537', '9339f82b36d4b8f0ba73936943f1da96'),
(11, '9', '91201pm1607', '41361a7c23d984a84f00ef641bb48508'),
(12, '10', '101201pm1623', 'e5be9f5494441d72d14f9d23b0e0b9ad'),
(13, 'ç', '1547331392', '0eb45ad37d9d2ef1ef090afce8aab176'),
(14, '11', '111201pm1739', 'a476c160b79655e7eb0650ad0100ed3f'),
(15, 'w', '1547331468', 'e0a5e3e6601f31f4e45ee07d8645e8cb'),
(16, 'grupormaker@gmail.com', '1547331677', '7614a0d717e49696255f7285bee55feb'),
(17, 'grupormaker@gmail.com', '1547331679', '469555dc05dd0b9d3bb360688deda3b2'),
(18, 'grupormaker@gmail.com', '1547331679', '469555dc05dd0b9d3bb360688deda3b2'),
(19, '12', '121201pm2516', '509dce30f1027bef5a283ce4eb54123c'),
(20, 'g', '1547331937', '92247b369dde2e5413a995914c3ee98e'),
(21, 'g', '1547331938', '438a45cb1b56c35b8af244c7a7257909'),
(22, 'grupormaker@gmail.com', '1547333580', 'bccd8c9de0b28e333e0b95ec8c512b8d'),
(23, 'grupormaker@gmail.com', '1547333641', 'dbac01f18283db58273ace187391706d'),
(24, 'grupormaker@gmail.com', '1547333641', 'dbac01f18283db58273ace187391706d'),
(25, 'grupormaker@gmail.com', '1547333650', 'b4f1405afaf58ef888aa09d946b16d7d'),
(26, 'grupormaker@gmail.com', '1547333664', 'e6cd98084dcbee38151246ce319b23c2'),
(27, 'grupormaker@gmail.com', '1547333722', 'a1982cbefe3b572e58086b88c773ff53'),
(28, 'grupormaker@gmail.com', '1547333740', '0a51727163a377536850b371f39103b9'),
(29, 'grupormaker@gmail.com', '1547333811', '56fb472927efee78171e5579b0a5867c'),
(30, 'grupormaker@gmail.com', '1547333833', '17a975af879f940152925b5602bdea76'),
(31, 'grupormaker@gmail.com', '1547333842', '5c0d3c8dd3840627b4a06f7fd75b5e3e'),
(32, 'grupormaker@gmail.com', '1547333862', '50ab070526a97ce978937c181b3748bc'),
(33, 'grupormaker@gmail.com', '1547333862', '50ab070526a97ce978937c181b3748bc'),
(34, 'grupormaker@gmail.com', '1547333904', '02bc89976f849d98ff4e6b495876699e'),
(35, 'grupormaker@gmail.com', '1547333914', 'ff7fb5d081b9c4392ea0c4073924f345'),
(36, '13', '131401pm1619', '3c7e9e592c00360d51de0973b1e1a5bd'),
(37, 'z', '1547493456', '59ba47ed27e67cd623ca46b6c0519e7b'),
(38, 'z', '1547493484', 'bc84b71949268f374c534244f20b9671'),
(39, 'z', '1547493515', '6c28f24dfca7ef1f71616933bc11ec5c'),
(40, 'z', '1547493534', 'bb091fdc3a23dcdab42efb6bce1cc0bf'),
(41, 'z', '1547493729', 'ac7fdad10fff6becc6f4c343890b240a'),
(42, 'z', '1547493743', '8ebc024a296bf35ccef9a3f2e84a85cd'),
(43, 'z', '1547493845', '5c3c44c25ca8e99c527e57129bf627cf'),
(44, 'z', '1547493854', '3be4a9b951b77aaa008e780cbc816fa2'),
(45, 'z', '1547493860', 'ed467735dea9eaed09dfc2e2f7936c14'),
(46, 'z', '1547493870', '71a79a5d1b3e57b9c73e65d9035a9842'),
(47, 'z', '1547493879', '3d1dbc06353430698875271121508aa3'),
(48, 'z', '1547493925', '3920a6d650678197cd85cce3e3d7b73b'),
(49, 'z', '1547494013', 'd55eb8463539c56fd881ac6d6226ca8e'),
(50, 'z', '1547494020', '112c53dedf18e4ef1bd186c7478a3a44'),
(51, 'z', '1547494158', '6e1db280be3c58d4b48e3dd45c971dac'),
(52, 'z', '1547494169', '7c2de22b0b97458ce2ff094a3d6b77b2'),
(53, 'z', '1547494247', 'bae7730ba96e22f8f300a3b0e69f4353'),
(54, 'z', '1547494253', '7676910790d180a09446d21db1904d80'),
(55, '14', '141401pm2154', 'b0ff20b2b0bfa5cf9a0809390d149161'),
(56, 'grupormaker@gmail.com', '1547552968', '4f1ecd42e285d72315bd774c5bf2125a'),
(57, 'a@b.com', '1547571881', '01ec164879f9e733d3e7f7ed247663a5'),
(58, 'a@b.com', '1547571925', 'f2b906463e86dfc01f098b774f0ef9f0'),
(59, 'a@b.com', '1547827042', '4e0ceaf766b1296b313d5c45828b9ef7'),
(60, '15', '152201pm1054', '45b463f0972c5bc92a584b58704a94e2'),
(61, '16', '162201pm1647', 'c7d73cb56d4e6a59626c1799129d4608'),
(62, '17', '172201pm1804', '74edec4b79246eab94190e9b7847e580'),
(63, '18', '182201pm1851', '58f3778190cc5a47ac7fae1560e7b2f0'),
(64, 'ok', '1548199448', 'b53d2d0b78ede9b178df8b383d2564ac');

-- --------------------------------------------------------

--
-- Estrutura para tabela `servicios`
--

CREATE TABLE `servicios` (
  `Id` int(11) NOT NULL,
  `Abr` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `Nombre` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `Descripcion` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IdServicioHijo` int(11) DEFAULT NULL,
  `IdTexto` int(11) DEFAULT NULL,
  `Orden` int(11) NOT NULL,
  `Grupo` int(11) NOT NULL,
  `Path` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `servicios`
--

INSERT INTO `servicios` (`Id`, `Abr`, `Nombre`, `Descripcion`, `IdServicioHijo`, `IdTexto`, `Orden`, `Grupo`, `Path`) VALUES
(0, 'S1', 'Hamburguer', NULL, 0, NULL, 0, 0, 'imagenesservicios1.png'),
(1, 'S2', 'Book', 'Descripcion 1', 0, 0, 1, 1, 'imagenesservicios2.png'),
(2, 'S3', 'Theater', 'Descripcion 2', 1, 0, 1, 2, 'imagenesservicios3.png'),
(3, 'S4', 'Bag', NULL, 2, NULL, 0, 0, 'imagenesservicios4.png'),
(4, 'S5', 'Chemical ', NULL, 2, NULL, 0, 0, 'imagenesservicios5.png'),
(5, 'S6', 'Car', NULL, 2, NULL, 0, 0, 'imagenesservicios6.png'),
(6, 'S7', 'Trash', NULL, 4, NULL, 0, 0, 'imagenesservicios7.png'),
(7, 'S8', 'Whater', NULL, 8, NULL, 0, 0, 'imagenesservicios8.png'),
(8, 'S9', 'Head', NULL, 8, NULL, 0, 0, 'imagenesservicios9.png'),
(9, 'S10', 'Sheets', NULL, 8, NULL, 0, 0, 'imagenesservicios10.png');

-- --------------------------------------------------------

--
-- Estrutura para tabela `servicioshijos`
--

CREATE TABLE `servicioshijos` (
  `Id` int(11) NOT NULL,
  `IdServicioPadre` int(11) NOT NULL,
  `IdServicioHijo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `servicioshijos`
--

INSERT INTO `servicioshijos` (`Id`, `IdServicioPadre`, `IdServicioHijo`) VALUES
(0, 1, 0),
(1, 0, 2),
(2, 0, 5),
(3, 2, 3),
(4, 2, 6),
(5, 1, 5),
(6, 3, 0),
(7, 4, 3),
(8, 4, 0),
(9, 5, 0),
(10, 5, 6),
(11, 6, 0),
(12, 6, 8),
(13, 7, 0),
(14, 7, 0),
(15, 8, 0),
(16, 8, 0),
(17, 9, 8),
(18, 9, 0),
(19, 10, 0),
(20, 10, 7),
(21, 11, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) NOT NULL,
  `password` varchar(191) NOT NULL,
  `permissions` text DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `first_name` varchar(191) DEFAULT NULL,
  `last_name` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `gender` varchar(191) DEFAULT NULL,
  `country` varchar(191) DEFAULT NULL,
  `state` varchar(191) DEFAULT NULL,
  `city` varchar(191) DEFAULT NULL,
  `address` varchar(191) DEFAULT NULL,
  `postal` varchar(191) DEFAULT NULL,
  `count_Contacto` int(11) NOT NULL DEFAULT 0,
  `fecha_Contacto` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `Telefono` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `permissions`, `last_login`, `first_name`, `last_name`, `created_at`, `updated_at`, `deleted_at`, `gender`, `country`, `state`, `city`, `address`, `postal`, `count_Contacto`, `fecha_Contacto`, `Telefono`) VALUES
(1, 'torontoestudios@gmail.com', '202cb962ac59075b964b07152d234b70', '1', NULL, 'Lucas', NULL, '2019-01-12 00:55:59', '2019-01-12 00:55:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '1900-01-01 00:00:00', '1126521118'),
(2, '555@555', '0a113ef6b61820daa5611c870ed8d5ee', '1', NULL, '555', NULL, '2019-01-12 00:57:02', '2019-01-12 00:57:02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '1900-01-01 00:00:00', '555'),
(3, '123@123.123', '15de21c670ae7c3f6f3f1f37029303c9', '1', NULL, 'Lucas', NULL, '2019-01-12 01:01:48', '2019-01-12 01:01:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '1900-01-01 00:00:00', '123'),
(4, 'sa', 'c12e01f2a13ff5587e1e9e4aedb8242d', '1', NULL, 'sa', NULL, '2019-01-12 01:05:10', '2019-01-12 01:05:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '1900-01-01 00:00:00', 'sa'),
(5, 'renenrenan.com', '202cb962ac59075b964b07152d234b70', '1', NULL, 'Lucas', NULL, '2019-01-12 03:20:09', '2019-01-12 03:20:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '1900-01-01 00:00:00', '123'),
(6, 'zz', '25ed1bcb423b0b7200f485fc5ff71c8e', '1', NULL, 'zz', NULL, '2019-01-12 15:56:33', '2019-01-12 15:56:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '1900-01-01 00:00:00', 'zz'),
(7, 'a@b.com', '4a8a08f09d37b73795649038408b5f33', '1', NULL, 'Antonio', NULL, '2019-01-12 18:50:34', '2019-01-12 18:50:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '1900-01-01 00:00:00', '6'),
(8, 'grupormaker@gmail.com', '202cb962ac59075b964b07152d234b70', '1', NULL, 'Rmaker', NULL, '2019-01-12 22:15:37', '2019-01-12 22:15:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '1900-01-01 00:00:00', '000'),
(9, '99', 'ac627ab1ccbdb62ec96e702f07f6425b', '1', NULL, 'a', NULL, '2019-01-12 22:16:07', '2019-01-12 22:16:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '1900-01-01 00:00:00', '1126521118'),
(10, 'ç', 'a151296c076e83387520ddab205d9fa1', '1', NULL, 'ç', NULL, '2019-01-12 22:16:23', '2019-01-12 22:16:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '1900-01-01 00:00:00', 'ç'),
(11, 'w', 'f1290186a5d0b1ceab27f4e77c0c5d68', '1', NULL, 'w', NULL, '2019-01-12 22:17:39', '2019-01-12 22:17:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '1900-01-01 00:00:00', 'w'),
(12, 'g', 'b2f5ff47436671b6e533d8dc3614845d', '1', NULL, 'g', NULL, '2019-01-12 22:25:16', '2019-01-12 22:25:16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '1900-01-01 00:00:00', 'g'),
(13, 'z', 'fbade9e36a3f36d3d676c1b808451dd7', '1', NULL, 'z', NULL, '2019-01-14 19:16:19', '2019-01-14 19:16:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '1900-01-01 00:00:00', 'z'),
(14, 'b', '92eb5ffee6ae2fec3ad71c777531578f', '1', NULL, 'b', NULL, '2019-01-14 23:21:54', '2019-01-14 23:21:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '1900-01-01 00:00:00', 'b'),
(15, 'lucas@gmail.com', '202cb962ac59075b964b07152d234b70', '1', NULL, 'Lucas', NULL, '2019-01-22 23:10:54', '2019-01-22 23:10:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '1900-01-01 00:00:00', '1126'),
(16, 'ok', '444bcb3a3fcf8389296c49467f27e1d6', '1', NULL, 'ok', NULL, '2019-01-22 23:16:47', '2019-01-22 23:16:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '1900-01-01 00:00:00', 'ok'),
(17, 'ss', '3691308f2a4c2f6983f2880d32e29c84', '1', NULL, 'ss', NULL, '2019-01-22 23:18:04', '2019-01-22 23:18:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '1900-01-01 00:00:00', 'ss'),
(18, 'jaja', 'bb0ed6ad56f41c6de469776171261226', '1', NULL, 'jaja', NULL, '2019-01-22 23:18:51', '2019-01-22 23:18:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '1900-01-01 00:00:00', 'jaja');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `email_actions`
--
ALTER TABLE `email_actions`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Índices de tabela `role_users`
--
ALTER TABLE `role_users`
  ADD PRIMARY KEY (`user_id`,`role_id`);

--
-- Índices de tabela `section_login`
--
ALTER TABLE `section_login`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `servicios`
--
ALTER TABLE `servicios`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `UK_Servicios_Abr` (`Abr`),
  ADD KEY `Ind_GrupoOrden` (`Grupo`,`Orden`) USING BTREE,
  ADD KEY `FK_Texto` (`IdTexto`) USING BTREE,
  ADD KEY `FK_ServicioHijo` (`IdServicioHijo`) USING BTREE;

--
-- Índices de tabela `servicioshijos`
--
ALTER TABLE `servicioshijos`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_ServicioHijo` (`IdServicioHijo`) USING BTREE;

--
-- Índices de tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `email_actions`
--
ALTER TABLE `email_actions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `section_login`
--
ALTER TABLE `section_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `servicioshijos`
--
ALTER TABLE `servicioshijos`
  ADD CONSTRAINT `FK_ServicioHijo` FOREIGN KEY (`IdServicioHijo`) REFERENCES `servicios` (`Id`),
  ADD CONSTRAINT `FK_ServicioPadre` FOREIGN KEY (`IdServicioPadre`) REFERENCES `servicios` (`Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
